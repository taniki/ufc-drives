# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% id="da67vhW9Onss"
import requests
import pandas as pd
from bs4 import BeautifulSoup


# %% [markdown] id="yXyo2GY3oDTl"
# ## Liste des villes

# %% id="K2JnO2JooMmS"
def get_cities():
  url_cities = 'https://www.quechoisir.org/carte-interactive-drives-n21243/drive-ville-dvl/'
  res = requests.get(url_cities)
  html = BeautifulSoup(res.text)
  html_cities_a = html.find_all("a", href=lambda href: href and "/carte-interactive-drives-n21243/drive-ville" in href)
  return [ (tag.text.strip(), tag.get('href').split('-')[-1][2:-1]) for tag in html_cities_a ]

villes = pd.DataFrame(get_cities(), columns=['ville', 'code_insee'])

villes

# %%
villes.to_csv('../datasets/cities.csv', index=False)
