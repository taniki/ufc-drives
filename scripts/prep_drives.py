# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% id="da67vhW9Onss"
import requests
import pandas as pd
from tqdm import tqdm

# %% [markdown] id="yXyo2GY3oDTl"
# ## Liste des villes

# %% id="K2JnO2JooMmS"
villes = pd.read_csv('../datasets/cities.csv')


# %% [markdown] id="gzBHkv0Wn-eN"
# ## Statistiques et liste des drives autour d'une ville

# %% id="M0DBxEOAOrPq"
def get_drive(city, communeId, profil, radius):
  url = 'https://www.quechoisir.org/ajax/drives/getdrives/'

  parameters = {
      'zipCodeOrCity': city,
      'communeId': communeId,
      'profil': profil, # celibataire, famille
      'radius': radius # 5, 10, 15, 20
  }

  return requests.post(url, data=parameters)

test = get_drive('Lorient (72)', '56121', 'famille', 20).json() #['drive']['drives']

# %% colab={"base_uri": "https://localhost:8080/", "height": 1000} id="OcD78C_hO1PL" outputId="d05bd29c-9bbb-4143-8bf0-d4a8b9a183e3"
pd.DataFrame.from_records(test['drive']['drives'])


# %% [markdown] id="kDemKnaCtJDM"
# ## Liste de tous les drives

# %% colab={"base_uri": "https://localhost:8080/"} id="fFoERVUaRbzs" outputId="0ecc94b9-8cd7-499c-d8c4-5fa33e03116b"
def get_drives():
  return (
    pd
    .concat([

      pd
      .DataFrame
      .from_records(
          get_drive(ville, code, profil, 20)
          .json()
          ['drive']
          ['drives']
      )
      .assign(profil = profil)

      for (i, ville, code) in tqdm(villes.to_records())
      for profil in ["celibataire", "couple", "famille"]
    ])
    .drop_duplicates(subset=['id', 'profil'])
  )

drives = get_drives()
drives.shape

# %%
drives.to_csv('../datasets/drives.csv')

# %%
